# Travel Salesman Problem in PHP - 32 cities 

## Challenge guidelines

Given a list of companies GPS locations, write a script that will help you find the shortest path to visit all 32
places once.
The list will being in "Beijing", you must begin your route there.
The maximum execution time is 15 minutes.

## Requirements

* Php

## Instructions

#### Install

Clone this app then cd into the folder

#### Launch

php -f solve.php

#### Test

php -f tests/all_tests.php

## Copyright and license

This software is registered under the [ISC license](https://opensource.org/licenses/ISC) 
Copyright(c) 2018 Ernest Conill