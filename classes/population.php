<?php
require_once('route.php');

class Population {

    private $routes = [];

    public function __construct($params)
    {
        extract($params);

        if (!isset($populationSize) || !is_integer($populationSize) || !$populationSize) {
            throw new Exception('Population should have a minimum size of 1');
        }

        if (!isset($cities) || !is_array($cities)) {
            throw new Exception('Population requires a list of cities');
        }

        $startFromTheFirst = isset($startFromTheFirst) ? $startFromTheFirst : false;
        $initialize = isset($initialize) ? $initialize : false;

        for ($i = 0; $i < $populationSize; $i++) {
            $newRoute = new Route($cities, $initialize, $startFromTheFirst);
            $this->saveRoute($i, $newRoute);
        }
    }

    // returns a route positioned as $position or a random route
    public function getRoute($position = null)
    {
        if (isset($position) && (!is_integer($position) || $position >= $this->size())) {
            throw new Exception('Invalid position.');
        }

        $index = (!isset($position)) ? mt_rand(0, $this->size()-1) : $position;
        return $this->routes[$index];
    }

    public function getFittest() {
        $fittest = $this->routes[0];

        for ($i = 1; $i < $this->size(); $i++) {
            if ($fittest->getFitness() < $this->getRoute($i)->getFitness()) {
                $fittest = $this->getRoute($i);
            }
        }

        return $fittest;
    }

    public function saveRoute($position, Route $route)
    {
        if (!isset($position) || !is_integer($position) || $position > $this->size()) {
            throw new Exception('Invalid position.');
        }

        $this->routes[$position] = $route;
    }

    public function size()
    {
        return count($this->routes);
    }
}