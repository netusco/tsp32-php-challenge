<?php

Class Route {

    private $cities = [];
    private $fitness = 0;
    private $distance = 0;

    /**
     * Route constructor.
     * @param array $cities
     * @param bool $generateIndividual
     * @param bool $startFromTheFirst
     * @throws Exception
     * @internal param array $cities of City elements
     */
    public function __construct($cities, $generateIndividual = false, $startFromTheFirst = false)
    {
        if (empty($cities)) {
            throw new Exception('You should create a Route from an array of cities.');
        }

        $this->cities = $cities;

        if ($generateIndividual) {
            if ($startFromTheFirst) {
                $first = array_shift($this->cities);
                shuffle($this->cities);
                array_unshift($this->cities, $first);
            } else {
                shuffle($this->cities);
            }
        }
    }

    public function getCity($position)
    {
        if (!is_int($position) || $position > $this->size()) {
            throw new Exception('Invalid position given');
        }

        return $this->cities[$position];
    }

    public function getAllCities()
    {
        return $this->cities;
    }

    /**
     * @param City $city
     * @param array $range Range of positions to search within
     * @return integer position or false
     */
    public function getCityPosition(City $city, $range = null)
    {
        if (is_array($range) && empty($range)) {
            return false;
        }

        $cities = (!is_null($range)) ? array_intersect_key($this->cities, array_flip($range)) : $this->cities;

        return array_search($city, $cities, true);
    }

    public function setCity($position, City $city)
    {
        if (!is_int($position) || $position > $this->size()) {
            throw new Exception('Invalid position given');
        }

        $this->cities[$position] =  $city;
    }

    public function getFitness()
    {
        if ($this->fitness == 0) {
            $this->fitness = 1 / $this->getDistance();
        }

        return $this->fitness;
    }

    public function getDistance()
    {
        $this->distance = 0;

        foreach($this->cities as $key => $city) {
            $next = ($key >= $this->size()-1) ? 0 : $key+1;
            $this->distance += $city->distanceTo($this->cities[$next]);
        }

        return $this->distance;
    }

    public function size()
    {
        return count($this->cities);
    }
}