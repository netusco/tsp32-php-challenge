<?php

class CityList {

    static public $cities = [];

    static public function addCity(City $city)
    {
        array_push(self::$cities, $city);
    }

    static public function getCity($position)
    {
        if (empty(self::$cities)) {
            throw new Exception('There is no cities to get.');
        }

        if (!isset($position) || !is_integer($position) || $position >= count(self::$cities)) {
            throw new Exception('Invalid position.');
        }

        return self::$cities[$position];
    }

    static public function removeAllCities()
    {
        self::$cities = [];
    }

    static public function printCities($cities = [], $title = '')
    {
        $cities = empty($cities) ? self::$cities : $cities;

        echo PHP_EOL.$title.PHP_EOL;
        foreach ($cities as $city) {
            echo $city->name.PHP_EOL;
        }
        echo PHP_EOL;
    }
}