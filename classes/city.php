<?php

class City {

    public $name;
    public $lat;
    public $lon;

    public function __construct($name, $lat, $lon)
    {
        if (!is_string($name) || !is_numeric($lat) || !is_numeric($lon)) {
            throw new Exception("Wrong parameters to create a City");
        }

        $this->name = $name;
        $this->lat = $lat + 0; // +0 for casting numeric strings
        $this->lon = $lon + 0;
    }

    // get the latitude of a city
    public function getLat()
    {
        return $this->lat;
    }

    // get the longitude of a city
    public function getLong()
    {
        return $this->lon;
    }

    // distance to another city in kms taking account of the roundish nature of the Earth
    public function distanceTo(City $city)
    {

        $pi80 = M_PI / 180;
        $lat1 = $this->lat * $pi80;
        $lon1 = $this->lon * $pi80;
        $lat2 = $city->lat * $pi80;
        $lon2 = $city->lon * $pi80;

        $radius = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $b = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km = $radius * $b;

        return (double)$km;
    }
}