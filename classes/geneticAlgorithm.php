<?php
require_once('population.php');
require_once('route.php');

Class GeneticAlgorithm {

    // Rate % of random values mutated on each Route
    public static $mutationRate = 0.020;

    // Crossover number of routes for each pool
    public static $poolSize = 7;

    // To keep the best route on each evolution
    public static $elitism = true;


    static public function evolvePopulation(Population $population, $startFromTheFirst = false)
    {
        if (!is_bool($startFromTheFirst)) {
            throw new Exception('Invalid parameter given.');
        }

        $params = [
            'cities' => $population->getFittest()->getAllCities(),
            'populationSize' => $population->size(),
            'startFromTheFirst' => $startFromTheFirst,
            'initialize' => false,
        ];
        $newPopulation = new Population($params);

        // keep our best route no need to save it as we create the pool out of the fittest
        $offset = (self::$elitism) ? 1 : 0;

        // Crossover population
        for ($i = $offset, $j = $newPopulation->size(); $i < $j; $i++) {
            $indRoute1 = self::poolSelection($population);
            $indRoute2 = self::poolSelection($population);
            $newIndRoute = self::crossover($indRoute1, $indRoute2, $startFromTheFirst);
            $newPopulation->saveRoute($i, $newIndRoute);
        }

        // Mutate the new population a bit to add some new genetic material
        for ($i = $offset, $j = $newPopulation->size(); $i < $j; $i++) {
            self::mutate($newPopulation->getRoute($i), $startFromTheFirst);
        }

        return $newPopulation;
    }

    static private function poolSelection(Population $population)
    {
        $params = [
            'cities' => $population->getFittest()->getAllCities(),
            'populationSize' => self::$poolSize,
        ];
        $pool = new Population($params);

        // Get a random route for each pool
        for ($i = 0; $i < self::$poolSize; $i++) {
            $pool->saveRoute($i, $population->getRoute());
        }

        return $pool->getFittest();
    }

    // Crossover algorithm given 2 Routes
    static private function crossover(Route $indRoute1, Route $indRoute2, $startFromTheFirst)
    {
        if (!isset($startFromTheFirst) || !is_bool($startFromTheFirst)) {
            throw new Exception('Invalid parameter given.');
        }

        if ($indRoute1->size() !== $indRoute2->size()) {
            throw new Exception('Crossover expects individuals with the same size.');
        }

        $newIndRoute = new Route($indRoute1->getAllCities());

        if ($indRoute1->getAllCities() === $indRoute2->getAllCities()) {
            return $newIndRoute;
        }

        $index1 = $index2 = null;
        $startIdx = ($startFromTheFirst) ? 1 : 0;

        while ($index1 === $index2) {
            $index1 = mt_rand($startIdx, $indRoute1->size()-1);
            $index2 = mt_rand($startIdx, $indRoute1->size()-1);
        }

        $intIdxRange = range(min($index1, $index2), max($index1, $index2));

        if ($startFromTheFirst) {
            array_unshift($intIdxRange, 0);
        }

        $extIdxRange = array_diff(range(0, $newIndRoute->size()-1), $intIdxRange);

        // fill up with cities from $indRoute2 that doesn't exist within the range $intIdxRange
        for ($i = 0; $i < $indRoute2->size(); $i++) {
            $position = $newIndRoute->getCityPosition($indRoute2->getCity($i), $intIdxRange);

            if ($position === false) {
                $extPos = array_shift($extIdxRange);

                if (is_null($extPos)) {
                    break;
                }

                $newIndRoute->setCity($extPos, $indRoute2->getCity($i));
            } else {
                // removing the index found to optimize the search.
                array_splice($intIdxRange, array_search($position, $intIdxRange ), 1);
            }
        }

        return $newIndRoute;
    }

    static private function mutate(Route $route, $startFromTheFirst)
    {
        if (!isset($startFromTheFirst) || !is_bool($startFromTheFirst)) {
            throw new Exception('Invalid parameter given.');
        }

        $offset = ($startFromTheFirst) ? 1 : 0;

        for ($pos1 = $offset; $pos1 < $route->size(); $pos1++) {

            if (self::random() < self::$mutationRate) {
                if ($startFromTheFirst) {
                    $pos2 = 0;
                    while ($pos2 < 1) {
                        $pos2 = (int) ($route->size() * self::random());
                    }
                }

                $city1 = $route->getCity($pos1);
                $city2 = $route->getCity($pos2);

                $route->setCity($pos2, $city1);
                $route->setCity($pos1, $city2);
            }
        }
    }

    static private function random()
    {
        return mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();
    }
}