<?php
require_once('cityList.php');
require_once('population.php');
require_once('city.php');
require_once('geneticAlgorithm.php');


Class TSP_Ctrl {

    // Number of unchanged routes before ending
    protected $maxStagnation = 50000;

    // Number of population
    protected $population = 50;

    // Maximum time running the script (in seconds)
    protected $maxTimeRunning = 30;

    // Start fom the first element
    protected $startFromTheFirst= false;

    // File path where all cities are
    protected $filePath = 'cities.txt';

    // Switch debug mode
    protected $debug = false;


    public function __construct($opts = [])
    {
        foreach ($opts as $key => $value) {
            if (isset($this->{$key})) {
                $this->{$key} = $value;
            }
        }
    }

    public function init()
    {
        $initTime = microtime(true);
        $stagnations = 0;
        $params = [
            'cities' => $this->getAllCities(),
            'populationSize' => $this->population,
            'startFromTheFirst' => $this->startFromTheFirst,
            'initialize' => true,
        ];
        $population = new Population($params);
        $lastDistance = $population->getFittest()->getDistance();
        if ($this->debug) echo 'Initial distance: '. $lastDistance.PHP_EOL;

        // Evolve population until reaching maxStagnation or maxTimeRunning
        while ($stagnations < $this->maxStagnation) {
            $population = GeneticAlgorithm::evolvePopulation($population, $this->startFromTheFirst);
            $distance = $population->getFittest()->getDistance();
            $stagnations = ($distance === $lastDistance) ? $stagnations + 1 : 0;
            $lastDistance = $distance;
            if ((microtime(true) - $initTime) > $this->maxTimeRunning) break;
        }

        if ( $this->debug && (microtime(true) - $initTime) >= $this->maxTimeRunning ) {
            echo 'Reached maxTimeRunning of '.$this->maxTimeRunning.' seconds'.PHP_EOL;
            echo 'Stagnation: '.$stagnations.PHP_EOL;
        }

        if ( $this->debug && $stagnations >= $this->maxStagnation ) {
            echo 'Reached maxStagnation of '.$this->maxStagnation.' times having the same result'.PHP_EOL;
            echo 'Timing: '.(microtime(true) - $initTime).PHP_EOL;
        }

        if ($this->debug) print('Evolved distance: '. $lastDistance.PHP_EOL);

        return $population->getFittest()->getAllCities();
    }

    public function getAllCities()
    {
        // reset cities before adding any new one
        CityList::removeAllCities();

        $file = fopen($this->filePath, 'r');

        while (!feof($file)) {
            $line = rtrim(fgets($file));

            // avoid empty lines
            if (empty($line)) continue;

            $data = explode(' ', $line);
            $lon = array_pop($data);
            $lan = array_pop($data);
            $name = join(' ', $data);
            CityList::addCity(new City($name, $lan, $lon));
        }

        fclose($file);

        return CityList::$cities;
    }
}