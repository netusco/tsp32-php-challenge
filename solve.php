<?php
require_once ('classes/cityList.php');
require_once ('classes/tspCtrl.php');

$opts = [
    'maxStagnation' => 2500000,
    'population' => 35,
    'maxTimeRunning' => 900,
    'startFromTheFirst' => true,
    'filePath' => 'cities.txt',
    'debug' => false,
];

$tsp = new TSP_Ctrl($opts);
$result = $tsp->init();
CityList::printCities($result);