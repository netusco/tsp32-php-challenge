<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../classes/tspCtrl.php');

class TestOfPopulation extends UnitTestCase {

    private $cities;
    private $population;
    private $params;

    function setUp()
    {
        $tsp = new TSP_Ctrl();
        $this->cities = $tsp->getAllCities();
        $this->params = [
            'cities' => $this->cities,
            'populationSize' => 40,
            'startFromTheFirst' => true,
            'initialize' => true,
        ];
        $this->population = new Population($this->params);
    }

    function testFailedPopulationCreation1()
    {
        $this->expectException(new Exception('Population should have a minimum size of 1'));
        unset($this->params['populationSize']);
        new Population($this->params);
    }

    function testFailedPopulationCreation2()
    {
        $this->expectException(new Exception('Population should have a minimum size of 1'));
        $this->params['populationSize'] = 0;
        new Population($this->params);
    }

    function testFailedPopulationCreation3()
    {
        $this->expectException(new Exception('Population requires a list of cities'));
        unset($this->params['cities']);
        new Population($this->params);
    }

    function testFailGetRoute1()
    {
        $this->expectException(new Exception('Invalid position.'));
        $this->population->getRoute($this->population->size() + 1);
    }

    function testFailGetRoute2()
    {
        $this->expectException(new Exception('Invalid position.'));
        $this->population->getRoute('abc');
    }

    function testGetRoute()
    {
        $this->assertIsA($this->population->getRoute(), 'Route');
    }

    function testCreatePopulationStartFromTheFirst()
    {
        // a random route should have the first city equal
        $this->assertIdentical($this->population->getRoute()->getCity(0), $this->cities[0]);
    }

    function testGetFittest()
    {
        $fittest = $this->population->getFittest();
        $first = $this->population->getRoute(0);
        $randRoute = $this->population->getRoute();

        $this->assertTrue($first->getFitness() <= $fittest->getFitness());
        $this->assertTrue($randRoute->getFitness() <= $fittest->getFitness());
        $this->assertIsA($fittest, 'Route');
    }

    function testFailSaveRoute1()
    {
        $this->expectException(new Exception('Invalid position.'));
        $this->population->saveRoute($this->population->size() + 1, $this->population->getRoute(0));
    }

    function testFailSaveRoute2()
    {
        $this->expectException(new Exception('Invalid position.'));
        $this->population->saveRoute($this->population->size() + 1, $this->population->getRoute(0));
    }

    function testSaveRoute()
    {
        $size = $this->population->size();
        $this->population->saveRoute($size, $this->population->getRoute(0));
        $this->assertIdentical($this->population->getRoute(0), $this->population->getRoute($size));
        $this->assertIdentical($size + 1, $this->population->size());
    }

    function testSize()
    {
        $this->assertIdentical($this->params['populationSize'], $this->population->size());
    }
}