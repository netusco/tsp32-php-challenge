<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../classes/tspCtrl.php');

class TestOfTspCtrl extends UnitTestCase {

    function testGetAllCities()
    {
        $tspCtrl = new TSP_Ctrl();
        $cities = $tspCtrl->getAllCities();
        $this->assertIsA($cities, 'Array');

        // 32 is the number of cities given and stored in cities.txt file
        $this->assertIdentical(32, count($cities));
    }

    function testInit()
    {
        $opts = [
            'maxStagnation' => 1500,
            'population' => 25,
            'maxTimeRunning' => 1,
            'startFromTheFirst' => true,
        ];

        $tspCtrl = new TSP_Ctrl($opts);
        $cities1 = $tspCtrl->getAllCities();
        $route1 = new Route($cities1);
        ob_start(); // omit console prints
        $cities2 = $tspCtrl->init();
        ob_clean();
        $route2 = new Route($cities2);

        $this->assertIdentical(count($cities1), count($cities2));
        $this->assertIdentical($cities1[0], $cities2[0]);
        $this->assertTrue($route1->getDistance() > $route2->getDistance());
    }
}