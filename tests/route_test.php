<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../classes/tspCtrl.php');
require_once(dirname(__FILE__) . '/../classes/route.php');

class TestOfRoute extends UnitTestCase {

    private $cities;
    private $route;

    function setUp()
    {
        $tsp = new TSP_Ctrl();
        $this->cities = $tsp->getAllCities();
        $this->route = new Route($this->cities);
    }

    function testFailedRouteCreation()
    {
        $this->expectException(new Exception("You should create a Route from an array of cities."));
        new Route(null);
    }

    function testFailedGetCity1()
    {
        $this->expectException(new Exception("Invalid position given"));
        $this->route->getCity('abc');
    }

    function testFailedGetCity2()
    {
        $this->expectException(new Exception("Invalid position given"));
        $this->route->getCity($this->route->size() + 1);
    }

    function testGetCity()
    {
        $this->assertIdentical($this->cities[10], $this->route->getCity(10));
    }

    function testGetAllCities()
    {
        $this->assertIdentical($this->cities, $this->route->getAllCities());
    }

    function testGenerateIndividualCreation()
    {
        $route = new Route($this->cities, true);
        $cities = $route->getAllCities();

        $this->assertNotIdentical($cities, $this->cities);
    }

    function testStartFromTheFirstCreation()
    {
        $route = new Route($this->cities, true, true);
        $cities = $route->getAllCities();

        $this->assertNotIdentical($cities, $this->cities);
        $this->assertIdentical($cities[0], $this->cities[0]);
    }

    function testFailedSetCity1()
    {
        $this->expectException(new Exception("Invalid position given"));
        $this->route->setCity('abc', $this->cities[0]);
    }

    function testFailedSetCity2()
    {
        $this->expectException(new Exception("Invalid position given"));
        $this->route->setCity($this->route->size() + 1, $this->cities[0]);
    }

    function testSetCity()
    {
        $this->assertNotIdentical($this->route->getCity(0), $this->route->getCity(10));

        $this->route->setCity(0, $this->route->getCity(10));
        $this->assertIdentical($this->route->getCity(0), $this->route->getCity(10));
    }

    function testGetDistance()
    {
        // around 240000 is the distance of the given city list, change this number if the list changes
        $this->assertWithinMargin(240000, $this->route->getDistance(), 5000);
    }

    function testSize()
    {
        $this->assertIdentical(count($this->cities), $this->route->size());
    }

    function testGetFitness()
    {
        // using 240000 as the distance of the given city list, change this number if the list changes
        $this->assertWithinMargin(1/240000, $this->route->getFitness(), 0.0000001);
    }

    function testGetCityPosition()
    {
        $this->assertIdentical(10, $this->route->getCityPosition($this->cities[10]));
    }

    function testGetCityPositionRange()
    {
        $this->assertIdentical(10, $this->route->getCityPosition($this->cities[10], [8,9,10]));
        $this->assertFalse($this->route->getCityPosition($this->cities[10], [6,7,8,9]));
        $this->assertFalse($this->route->getCityPosition($this->cities[10], []));
    }
}