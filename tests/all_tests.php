<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once('city_test.php');
require_once('cityList_test.php');
require_once('route_test.php');
require_once('population_test.php');
require_once('geneticAlgorithm_test.php');
require_once('tspCtrl_test.php');


class AllTests extends TestSuite {

    function __construct()
    {
        parent::__construct();
        //$this->add(new TestOfCity());
    }
}