<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../classes/tspCtrl.php');

class TestOfGeneticAlgorithm extends UnitTestCase {

    private $cities;
    private $population;
    private $params;

    function setUp()
    {
        $tsp = new TSP_Ctrl();
        $this->cities = $tsp->getAllCities();
        $this->params = [
            'cities' => $this->cities,
            'populationSize' => 40,
            'startFromTheFirst' => true,
            'initialize' => true,
        ];
        $this->population = new Population($this->params);
    }

    function testFailEvolvePopulation1()
    {
        $this->expectException(new Exception('Invalid parameter given.'));
        GeneticAlgorithm::evolvePopulation($this->population, 'abc');
    }

    function testEvolvePopulation()
    {
        $population = GeneticAlgorithm::evolvePopulation($this->population, $this->params['startFromTheFirst']);

        $this->assertIdentical($this->population->size(), $population->size());
        $this->assertNotIdentical($this->population, $population);
    }
}