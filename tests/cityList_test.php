<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../classes/cityList.php');
require_once(dirname(__FILE__) . '/../classes/tspCtrl.php');

class TestOfCityList extends UnitTestCase {

    private $cities;

    function setUp()
    {
        $tsp = new TSP_Ctrl();
        $this->cities = $tsp->getAllCities();
    }

    function testAddCity() {
        $size = count($this->cities);
        CityList::addCity($this->cities[0]);
        $this->assertIdentical($this->cities[0], CityList::$cities[count(CityList::$cities) - 1]);
        $this->assertIdentical($size + 1, count(CityList::$cities));
    }

    function testRemoveAllCities() {
        CityList::removeAllCities();
        $this->assertTrue(count(CityList::$cities) === 0);
    }

    function testFailGetCity1() {
        $this->expectException(new Exception('There is no cities to get.'));
        CityList::removeAllCities();
        CityList::getCity(10);
    }

    function testFailGetCity2() {
        $this->expectException(new Exception('Invalid position.'));
        CityList::getCity(null);
    }

    function testFailGetCity3() {
        $this->expectException(new Exception('Invalid position.'));
        CityList::getCity('abc');
    }

    function testFailGetCity4() {
        $this->expectException(new Exception('Invalid position.'));
        CityList::getCity(count(CityList::$cities));
    }

    function testGetCity() {
        $this->assertIdentical($this->cities[10], CityList::getCity(10));
    }
}