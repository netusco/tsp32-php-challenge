<?php
require_once(dirname(__FILE__) . '/../simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../classes/city.php');

class TestOfCity extends UnitTestCase {

    private $city;

    function setUp()
    {
        $this->city = new City('Lima', -12, -77.2);
    }

    function testCorrectCityCreation()
    {
        $this->assertIdentical('Lima', $this->city->name);
        $this->assertIdentical(-12, $this->city->lat);
        $this->assertIdentical(-77.2, $this->city->lon);
    }

    function testFailedCityCreation1()
    {
        $this->expectException(new Exception("Wrong parameters to create a City"));
        new City(null, 12, -77);
    }

    function testFailedCityCreation2()
    {
        $this->expectException(new Exception("Wrong parameters to create a City"));
        new City('Lima', null, -77);
    }

    function testCityDistanceTo() {
        $city2 = new City('Cusco', -13.53, -71.97);

        // taking 585 kms using googlemaps distance mesurement tool
        $this->assertWithinMargin(585, $this->city->distanceTo($city2), 20);
    }
}